
/**
 * Name: Pitt Street
 * Date created: 7/05/16
 * Version: 1.0
 */


package com.apps.pittstreet.vrpittstreet.StepDetection;
import android.os.AsyncTask;
import android.os.Looper;
import com.apps.pittstreet.vrpittstreet.MainActivity;

//This class is responsible for managing all background tasks
public class MainProcess extends AsyncTask<String, Integer, Boolean> {

    MainActivity mainActivity;
    StepDetectionSensor step;

    public MainProcess(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    //Transmits to main phone when a foot step is detected
    private void detectFootSteps() {
        int count = 0;
        Looper.prepare();
        step = new StepDetectionSensor(mainActivity);
        step.startListening();
        while(mainActivity.isDetectMotion()) {
            if(step.isStepDetected()) {
                count++;
                mainActivity.mycount=count;
                publishProgress(count);
                mainActivity.SenData();

            }
        }
    }

    protected Boolean doInBackground(String... params) {
        detectFootSteps();
        return true;
    }

    protected void onProgressUpdate(Integer... progress) {
        mainActivity.getTxtNumber().setText(""+progress[0]);
    }

    protected void onPostExecute(Boolean result) {
        step.stopListening();
    }
}

