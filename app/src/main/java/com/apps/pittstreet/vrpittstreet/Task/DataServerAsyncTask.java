package com.apps.pittstreet.vrpittstreet.Task;

import android.database.CursorJoiner;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.pittstreet.vrpittstreet.MainActivity;
import com.apps.pittstreet.vrpittstreet.StepDetection.MainProcess;
import com.apps.pittstreet.vrpittstreet.StepDetection.StepDetectionSensor;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A simple server socket that accepts connection and writes some data on
 * the stream.
 */
public class DataServerAsyncTask extends
        AsyncTask<Void, String, String> {

    public TextView statusText;
    private MainActivity activity;
    private final ReentrantLock lock = new ReentrantLock();
    public final Condition tryAgain = lock.newCondition();
    private volatile boolean finished = false;
    private String qualifier ="false";
    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 85);
    /**
     * @param statusText
     */
    public DataServerAsyncTask(MainActivity activity, View statusText) {
        this.statusText = (TextView) statusText;
        this.activity=activity;
    }

    @Override
    protected String doInBackground(Void... params) {
        String str = "terminated";
        //Could be useful, if we use multiple threads
        lock.lock();
        Log.i("LOCK","The Lock is locked here");
        do{
            try {
                //May never jump out
                while(!qualifier.equals("true"))
                {
                    Log.i("xyz", "data doinback");
                    ServerSocket serverSocket = new ServerSocket(8888);
                    Log.i("xyz","串口创建完成");
                    Log.i("Socket","Trying to find a serversocket");
                    Socket client = serverSocket.accept();
                    Log.i("xyz","The request is accepted, there is a connection now");
                    InputStream inputstream = client.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    int i;
                    while ((i = inputstream.read()) != -1) {
                        baos.write(i);
                    }
                    str = baos.toString();
                    serverSocket.close();
                    Log.i("Socket","ServerScoketClosed");
                    if(!str.equals(""))
                    {
                        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                        publishProgress(str);
                    }
                }
                try{
                    tryAgain.await();
                }catch(InterruptedException e){
                    System.out.println(Thread.currentThread().getName()+" interrupted.");
                }
            } catch (IOException e) {
                Log.e("xyz", e.toString());
            }
        }while(!finished);
        lock.unlock();
        Log.i("LOCK","The lock is unlocked after while");
        return str;
    }
    //Now it is just printing out the count of step, later may be used for updating StreetView
    protected void onProgressUpdate(String... result)
    {
        // Treat this like onPostExecute(), do something with result
        // This is an example...
        statusText.setText("StepNum: "+result[0]);
        activity.mycount = Integer.parseInt(result[0]);
        Log.i("onProgress","DataShouldBeSent");
        try{
            if(!(result[0] instanceof  String)) {
                runAgain();
                Log.i("LOCKCONDITION", "runAgain method is called, tryAgain is signaled");
            }
        }catch(Exception e) {
            System.out.println(e);
        }

    }

    public void runAgain() {
        // Call this to request data from the server again
        tryAgain.signal();
    }
    /*
     * (non-Javadoc)
     *
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    //@Override
    //Instead we use onProgressUpdate Now
    protected  void onPostExecute() {

        //Log.i("xyz", "data onpost");
        //Toast.makeText(activity, "result"+result, Toast.LENGTH_SHORT).show();
        //statusText.setText("StepNum: "+result);

    }
    /*
     * (non-Javadoc)
     *
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {
    }

    public void terminateTask() {
        // The task will only finish when we call this method
        finished = true;
        lock.unlock();
        Log.i("LOCK","The lock is unlocked when terminate");
    }

    @Override
    protected void onCancelled() {
        // Make sure we clean up if the task is killed
        terminateTask();
    }

}