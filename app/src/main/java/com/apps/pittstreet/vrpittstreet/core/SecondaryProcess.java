/**
 * Created by Andrew on 3/06/2016.
 */

package com.apps.pittstreet.vrpittstreet.core;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.apps.pittstreet.vrpittstreet.MainActivity;
import com.apps.pittstreet.vrpittstreet.pano.StreetViewImage;

public class SecondaryProcess extends AsyncTask<Void, Integer, Void> {

    private MainActivity mainActivity;
    private Bitmap image;
    private int StepNum;
    public SecondaryProcess(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected Void doInBackground(Void... params) {

        while(mainActivity.getRunSecondaryProcess()) {
            if(mainActivity.getIsMove()) {
                Log.d("PANO", "Loading image...");
                image = new StreetViewImage(mainActivity.getNextPanoID()).getImage();
                Log.d("PANO", "Loading done!");
                mainActivity.updateVR(image);
                publishProgress(0);
            }
            else if(mainActivity.isFormed&mainActivity.isGroupowner)
            {
                runServer();
            }
        }
        return null;
    }

    protected void onProgressUpdate(Integer... progress) {
        if(progress[0] == 0) {
            mainActivity.setWalkEnabled(true);
        } else if(progress[0] == 35) {
            mainActivity.movePosition();
        }
    }

    protected void onPostExecute(Void result){
        Log.d("THREAD RELEASE", "Exiting Main Process");
    }

    public void runServer() {
         if(mainActivity.mycount!=StepNum) {
             StepNum = mainActivity.mycount;
             //35 is just an arbitrary number for step detected
             publishProgress(35);
         }
    }
}
