/**
 * Created by Andrew on 3/06/2016.
 */

package com.apps.pittstreet.vrpittstreet.pano;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class StreetViewImage {

    private final int HEIGHT = 4;
    private final int WIDTH = 7;
    private final int ZOOM = 3;
    private final int XWRAPCONST = 256;
    private final int YWRAPCONST = 384;
    private Bitmap result;

    public StreetViewImage(String panoID) {
        Bitmap image;
        result = Bitmap.createBitmap(WIDTH*512-XWRAPCONST, HEIGHT*512-YWRAPCONST, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(result);

        for(int y = 0; y < HEIGHT; y++) {
            for(int x = 0; x < WIDTH; x++) {
                String urlString = "http://maps.google.com/cbk?output=tile&panoid="+panoID+"&zoom="+ZOOM+"&x="+x+"&y="+y;
                try {
                    image = BitmapFactory.decodeStream((InputStream)new URL(urlString).getContent());
                    c.drawBitmap(image, x*512, y*512, null);
                } catch (MalformedURLException e) {
                    Log.e("URL", "Malformed");
                } catch (IOException e) {
                    Log.e("URL", "Input Stream Failed");
                }
            }
        }
    }

    public Bitmap getImage() {
        return result;
    }
}
